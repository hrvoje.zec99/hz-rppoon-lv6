using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv6
{
    class CareTaker
    {
        //3.zadatak
        Stack<Memento> Mementos;
        public CareTaker()
        {
            Mementos = new Stack<Memento>();
        }
        public Memento PreviousState()
        {
            if (Mementos.Count == 0)
            {
                Console.WriteLine("error");
                return null;
            }
            return Mementos.Pop();
        }
        public void StoreState(Memento memento)
        {
            Mementos.Push(memento);
        }
    }
}
