using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv6
{
    class Productcs
    {
        //2.zadatak
        public string Description { get; private set; }
        public double Price { get; private set; }
        public Productcs(string description, double price)
        {
            this.Description = description;
            this.Price = price;
        }
        public override string ToString()
        {
            return this.Description + ":\n" + this.Price;
        }
    }
}