using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv6
{
    class Box : IAbstractCollection
    {
        //2.zadatak
        private List<Productcs> products;
        public Box() { this.products = new List<Productcs>(); }
        public Box(List<Productcs> products)
        {
            this.products = new List<Productcs>(products.ToArray());
        }
        public void AddProduct(Productcs product) { this.products.Add(product); }
        public void RemoveProduct(Productcs product) { this.products.Remove(product); }

        public int Count { get { return this.products.Count; } }
        public Productcs this[int index] { get { return this.products[index]; } }
        public IAbstractIterator GetIterator() { return new Iterator(this); }
    }
}
