namespace RPPOON_lv6
{
    interface IAbstractIterator
    {
        //1.zadatak
        //Note First();
        //Note Next();
        //bool IsDone { get; }
        //Note Current { get; }

        //2.zadatak
        Productcs First();
        Productcs Next();
        bool IsDone { get; }
        Productcs Current { get; }
    }
}