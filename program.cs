using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv6
{
    class Program
    {
        private static IAbstractIterator iterator;
        static void Main(string[] args)
        {
            //1.zadatak
            //Notebook notebook = new Notebook();
            //notebook.AddNote(new Note("dan", "cetvrtak"));
            //notebook.AddNote(new Note("mjesec", "svibanj"));
            //notebook.AddNote(new Note("godina", "2020"));

            //Iterator iterator = new Iterator(notebook);


            //while (iterator.IsDone != true)
            //{
            //    iterator.Current.Show();
            //    iterator.Next();
            //}


            //2.zadatak
            Box box = new Box();
            box.AddProduct(new Productcs("Lopta", 250));
            box.AddProduct(new Productcs("Dres", 700));
            box.AddProduct(new Productcs("Patike", 530));

            iterator = box.GetIterator();
            while (iterator.IsDone != true)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
                Console.WriteLine();
            }
            //3.zadatak
            CareTaker careTaker = new CareTaker();
            ToDoItem toDoItem = new ToDoItem("Real je najbolji", "nije", DateTime.Now);

            careTaker.StoreState(toDoItem.StoreState());
            toDoItem.Rename("Real je bolji od Barcelone");
            careTaker.StoreState(toDoItem.StoreState());
            toDoItem.Rename("Barcelona je bolja od Reala");
            Console.WriteLine(toDoItem.ToString());
            toDoItem.RestoreState(careTaker.PreviousState());
            Console.WriteLine(toDoItem.ToString());
            toDoItem.RestoreState(careTaker.PreviousState());
            Console.WriteLine(toDoItem.ToString());
        }
         
    }
}

